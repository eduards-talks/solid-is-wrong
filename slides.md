<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->

## SOLID is Wrong
<!-- .element: class="no-toc-progress" --> <!-- slide not in toc progress bar -->


----

## Now that is a provocative statement right?

- S
- O
- L
- I
- D

Note:
This talk works like this. I present each principle.
Tell you why I think it makes no sense, and propose an
alternative.

----  ----

## Single Responsibility

- one reason to change
- do only one thing

----

## Pointlessly Vague

- What is a single responsibility?
  - ETL is that one or three?
- How can you predict what will change?

----

## Write simple code

- Easy to reason about
- Can do several related things with ease
- Refactor until it "Fits into your head"

----  ----

## Open/Closed

- Open for extension
- Closed for modification
- When requirements change, add code, do not change code.

----

## Cruft Accretion

- The existing code is now wrong
- Replace it with code that works

----

## Write simple code

- It is easy to change
- It is easy to test
- It is both open and closed

----  ----

## Liskov Substitution

- Strong behavioral sub-typing
- Substitution with  a subtype preserves "desirable properties"
- "Provably undecidable" but useful

----

## Peter Drucker

- _There is nothing quite so useless, as doing with great efficiency, something that should not be done at all._
- Stuck in _is-a_/_has-a_ modelling mindset

----

## Write simple code

- How about: _acts-like-a_/_can-be-used-as-a_
- Composition is simpler than inheritance
- Avoid hierarchies where ever possible

----  ----

## Interface Segregation

- Many small interfaces are better than one big object
- Use small, roll-based interfaces
- No client depends on methods it does not use

----

## Stable Door

- Anything is better than one big object
- Use small, roll-based classes
- _No client depends on methods it does not use_ is true anyways

----

## Write simple code

- Do not write big objects to start with
- Make it "Fit in your head"
- If your class need many interfaces, simplify the class!

----  ----

## Dependency Inversion

- High level stuff should not depend on lower level stuff
- Abstractions (e.g. interfaces) should not depend on details (e.g. concrete implementations)

----

## Wrong Goal

- Reuse is overrated. Design for use!
- DIP introduces dependency on DI frameworks

----

## Write simple code

- You can get very far combining simple classes
- new is the new new
- Assemble small components that "Fit in your head"

----  ----

## SOLID

_Is just to much to remember. Just write simple code!_
